const gameMain = document.getElementById('game-main')

const makeReguladora = (pai) => {
  const reguladora = document.createElement('div')
  reguladora.classList.add('reguladora')
  pai.appendChild(reguladora)
}

function makeStandBy () {
  const standBy = document.createElement('div')
  standBy.classList.add('standBy')
  standBy.id = 'standBy'
  gameMain.appendChild(standBy)
  makeReguladora(standBy)
}

const makePiece = (pai) => {
  for(let i = 0; i < 4; i++) {
    const piece = document.createElement('div')
    piece.classList.add('pecas')
    piece.id = `peca-${4-i}`
    pai.appendChild(piece)
  }
}

const makeColunaStart = (pai) => {
  const colunaContainer = document.createElement('div')
  colunaContainer.classList.add('coluna-container')
  colunaContainer.id = 'coluna-start'
  pai.appendChild(colunaContainer)
  const gameColuna = document.createElement('div')
  gameColuna.classList.add('game-coluna')
  gameColuna.id = 'start'
  colunaContainer.appendChild(gameColuna)
  makeReguladora(gameColuna)
  makePiece(gameColuna)
}

const makeColunaOffSet = (pai) => {
  const colunaContainer = document.createElement('div')
  colunaContainer.classList.add('coluna-container')
  colunaContainer.id = 'coluna-offset'
  pai.appendChild(colunaContainer)
  const gameColuna = document.createElement('div')
  gameColuna.classList.add('game-coluna')
  gameColuna.id = 'offset'
  colunaContainer.appendChild(gameColuna)
  makeReguladora(gameColuna)
}

const makeColunaEnd = (pai) => {
  const colunaContainer = document.createElement('div')
  colunaContainer.classList.add('coluna-container')
  colunaContainer.id = 'coluna-end'
  pai.appendChild(colunaContainer)
  const gameColuna = document.createElement('div')
  gameColuna.classList.add('game-coluna')
  gameColuna.id = 'end'
  colunaContainer.appendChild(gameColuna)
  makeReguladora(gameColuna)
}
const makeGameContainer = () => {
  const gameContainer = document.createElement('div')
  gameContainer.classList.add('game-container')
  gameMain.appendChild(gameContainer)
  makeColunaStart(gameContainer)
  makeColunaOffSet(gameContainer)
  makeColunaEnd(gameContainer)
}

function MakeGame () {
  makeStandBy()
  makeGameContainer ()
}

MakeGame()





const peca1 = document.getElementById("peca-1");
const peca2 = document.getElementById("peca-2");
const peca3 = document.getElementById("peca-3");
const peca4 = document.getElementById("peca-4");
const reg = document.getElementById("reg");
const colunaStart = document.getElementById("start");
const colunaOffset = document.getElementById("offset");
const colunaEnd = document.getElementById("end");
const standBy = document.getElementById("standBy");
const resetButton = document.getElementById("reset");
const buttonResetModal = document.getElementById("button-reset-modal");
const modal = document.getElementById("modal");

let contador = 0;

buttonResetModal.addEventListener("click", function () {
  modal.classList.add("hidden");
  reset();
});

colunaStart.addEventListener("click", function () {
  if (contador === 0) {
    verificaColuna(this);
  } else {
    colocaPeca(this);
  }
});
colunaOffset.addEventListener("click", function () {
  if (contador === 0) {
    verificaColuna(this);
  } else {
    colocaPeca(this);
  }
});
colunaEnd.addEventListener("click", function () {
  if (contador === 0) {
    verificaColuna(this);
  } else {
    colocaPeca(this);
  }
});

resetButton.addEventListener("click", function () {
  reset();
});

function verificaColuna(elem) {
  const selecionado = elem.lastElementChild;
  const guardado = standBy.lastElementChild;

  if (standBy.childElementCount >= 1 && elem.childElementCount !== 1) {
    standBy.appendChild(selecionado);
    contador = 1;
  } else {
    alert("movimento inválido");
  }
}

function colocaPeca(elem) {
  const selecionado = elem.lastElementChild;
  const guardado = standBy.lastElementChild;
  if (
    elem.childElementCount >= 1 &&
    selecionado.clientWidth > guardado.clientWidth
  ) {
    elem.appendChild(guardado);
    contador = 0;
    setTimeout(function () {
      vitoria();
    }, 100);
  } else {
    alert("movimento inválido");
  }
}

function vitoria() {
  if (
    colunaOffset.childElementCount === 5 ||
    colunaEnd.childElementCount === 5
  ) {
    modal.classList.remove("hidden");
  }
}

function reset() {
  colunaStart.appendChild(peca4);
  colunaStart.appendChild(peca3);
  colunaStart.appendChild(peca2);
  colunaStart.appendChild(peca1);
  contador = 0;
}
